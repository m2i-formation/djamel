package tp_djamel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class tp_glpi {
	
	
	public static ChromeDriver driver;
	
	@BeforeClass
	public static void setup() {
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	
	@Test
	public void a_connection_glpi() {
		
		driver.get("https://demo.glpi-project.org/index.php");
		driver.findElement(By.id("login_name")).sendKeys("admin");
		driver.findElement(By.id("login_password")).sendKeys("admin");
		new Select(driver.findElement(By.name("language"))).selectByVisibleText("Fran�ais");
		
		
		driver.findElement(By.name("submit")).click();
		driver.findElement(By.id("menu1")).click();
		driver.findElementByXPath("//a[@title='Ajouter']").click();
		driver.findElementByLinkText("Gabarit vide").click();
		driver.findElement(By.name("name")).sendKeys("djamel85");
		driver.findElement(By.name("serial")).sendKeys("4070285");
		driver.findElement(By.name("comment")).sendKeys("bonjour,");
	
		
		
		driver.findElement(By.name("add")).click();
		driver.findElement(By.name("globalsearch")).sendKeys("djamel85");
		driver.findElement(By.name("globalsearchglass")).click();
		
		WebElement table = driver.findElement(By.xpath("//table[tbody/tr[1]/td[1]='djamel85']"));
		String nums = table.findElements(By.tagName("tr")).get(1).findElements(By.tagName("td")).get(4).getText();	
		System.out.println("le numero de serie est : "+nums);
		
		assertEquals ("4070285", nums);
		System.out.println("test modification");
		
		driver.findElementByXPath("//a[@title='D�connexion']").click();
		
		
		
	}

}
